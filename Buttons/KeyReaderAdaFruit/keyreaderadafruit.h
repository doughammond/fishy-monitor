#pragma once

#include "Buttons/keys.h"
#include "Peripherals/Adafruit_RGBLCDShield/Adafruit_RGBLCDShield.h"
#include "Peripherals/MCP23017/MCP23017.h"

class KeyReaderAdafruit
	: public Keys
{
public:
	KeyReaderAdafruit(I2C & in_cI2C);

	virtual uint8_t readButtons();

protected:
	MCP23017 m_cMCP;
	Adafruit_RGBLCDShield m_cLCD;

};