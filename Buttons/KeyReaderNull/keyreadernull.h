#pragma once

#include "keys.h"

class KeyReaderNull
    : public Keys
{
public:
                        KeyReaderNull(I2C & in_cI2C);
                            
    virtual uint8_t     readButtons();
};
