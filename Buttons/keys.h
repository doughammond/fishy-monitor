#pragma once

class Keys
{
public:
    enum EButtons
    {
        eButtonNone     = 0x00,
        eButtonUp       = 0x01,
        eButtonDown     = 0x02,
        eButtonLeft     = 0x04,
        eButtonRight    = 0x08,
        eButtonSelect   = 0x10,
    };
        
public:
                        Keys(I2C & in_cI2C);
                            
    virtual uint8_t     readButtons() = 0;
    
protected:
            I2C &       m_cI2C;
};
