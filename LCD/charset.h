#pragma once

namespace Chars {

static uint8_t emoticons[][8] = {
	// happy
	{
		0b000000,
		0b001010,
		0b000000,
		0b000100,
		0b000000,
		0b010001,
		0b001110,
		0b000000
	},

	// sad
	{
		0b000000,
		0b001010,
		0b000000,
		0b000100,
		0b000000,
		0b001110,
		0b010001,
		0b000000
	},

	// neutral
	{
		0b000000,
		0b001010,
		0b000000,
		0b000100,
		0b000000,
		0b000000,
		0b001110,
		0b000000
	}
};

enum Smileys
{
	Happy = 0,
	Sad,
	Neutral
};


static uint8_t fish[][8] = {
	// tail
	{
		0b001000,
		0b001100,
		0b001010,
		0b001001,
		0b001010,
		0b001100,
		0b001000,
		0b000000
	},

	// mid
	{
		0b000011,
		0b001111,
		0b010000,
		0b010000,
		0b010000,
		0b001000,
		0b000111,
		0b000011
	},

	// head
	{
		0b000000,
		0b011100,
		0b000010,
		0b001001,
		0b000001,
		0b001010,
		0b011100,
		0b000000
	},

};

enum Fish
{
	Tail = 0,
	Middle,
	Head
};

static uint8_t symbols1[][8] = {
	// degree
	{
		0xc,0x12,0x12,0xc,0x0,0x0,0x0,0x0 
	}
};

enum Symbols1
{
	Degree = 0
};

} // namespace