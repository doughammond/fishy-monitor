# GCC_BIN = /Users/doug/Applications/gcc-arm-none-eabi-5_2-2015q4/bin/
GCC_BIN = 
PROJECT = FishMonitor
BUILDDIR = _build/

# ./Buttons/KeyReaderAdaFruit/keyreaderadafruit.o
# ./Buttons/KeyReaderNull/keyreadernull.o
# ./Buttons/keys.o
# ./Peripherals/RTclock/RTclock.o
# ./Peripherals/RTclock/time_helper.o
# ./UI/MenuManager/MenuManager.o
# ./UI/Modules/DateModule.o
# ./UI/Modules/module.o
# ./UI/Modules/SyncModule.o
# ./UI/Modules/TempModule.o
# ./UI/Modules/TimeModule.o
# ./UI/Modules/TitleModule.o
OBJECTS = \
	./Peripherals/AdaFruit_RGBLCDShield/AdaFruit_RGBLCDShield.o \
	./Peripherals/DS18B20/DS18B20.o \
	./Peripherals/DS18B20/OneWireCRC.o \
	./Peripherals/DS18B20/OneWireThermometer.o \
	./Peripherals/MCP23017/MCP23017.o \
	./Util/DebugTrace.o \
	./main.o

SYS_OBJECTS = \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/board.o \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/cmsis_nvic.o \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/mbed_overrides.o \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/retarget.o \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/startup_MKL25Z4.o \
	./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/system_MKL25Z4.o

INCLUDE_PATHS = \
	-I. \
	-I./mbed \
	-I./mbed/TARGET_KL25Z \
	-I./mbed/TARGET_KL25Z/TARGET_Freescale \
	-I./mbed/TARGET_KL25Z/TARGET_Freescale/TARGET_KLXX \
	-I./mbed/TARGET_KL25Z/TARGET_Freescale/TARGET_KLXX/TARGET_KL25Z \
	-I./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM

LIBRARY_PATHS = -L./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM
LIBRARIES = -lmbed
LINKER_SCRIPT = ./mbed/TARGET_KL25Z/TOOLCHAIN_GCC_ARM/MKL25Z4.ld

###############################################################################
AS      = $(GCC_BIN)arm-none-eabi-as
CC      = $(GCC_BIN)arm-none-eabi-gcc
CPP     = $(GCC_BIN)arm-none-eabi-g++
LD      = $(GCC_BIN)arm-none-eabi-gcc
OBJCOPY = $(GCC_BIN)arm-none-eabi-objcopy
OBJDUMP = $(GCC_BIN)arm-none-eabi-objdump
SIZE    = $(GCC_BIN)arm-none-eabi-size


CPU = -mcpu=cortex-m0plus -mthumb 
CC_FLAGS = $(CPU) -c -g -fno-common -fmessage-length=0 -Wall -Wextra -fno-exceptions -ffunction-sections -fdata-sections -fomit-frame-pointer -MMD -MP
CC_SYMBOLS = -DTARGET_FF_ARDUINO -DTOOLCHAIN_GCC_ARM -DTARGET_KLXX -DTARGET_KL25Z -DTARGET_CORTEX_M -DTARGET_LIKE_MBED -DTARGET_M0P -DTARGET_Freescale -DMBED_BUILD_TIMESTAMP=1459363579.94 -D__MBED__=1 -D__CORTEX_M0PLUS -DTOOLCHAIN_GCC -DTARGET_LIKE_CORTEX_M0 -DARM_MATH_CM0PLUS 

LD_FLAGS = $(CPU) -Wl,--gc-sections --specs=nano.specs -Wl,--wrap,main -Wl,-Map=$(BUILDDIR)$(PROJECT).map,--cref
LD_FLAGS += -u _printf_float -u _scanf_float
LD_SYS_LIBS = -lstdc++ -lsupc++ -lm -lc -lgcc -lnosys


ifeq ($(DEBUG), 1)
  CC_FLAGS += -DDEBUG -O0
else
  CC_FLAGS += -DNDEBUG -Os
endif

.PHONY: all clean lst size

all: $(BUILDDIR)/$(PROJECT).bin $(BUILDDIR)/$(PROJECT).hex size


clean:
	rm -f $(BUILDDIR)/$(PROJECT).bin $(BUILDDIR)/$(PROJECT).elf $(BUILDDIR)/$(PROJECT).hex $(BUILDDIR)/$(PROJECT).map $(BUILDDIR)/$(PROJECT).lst $(OBJECTS) $(DEPS)


.asm.o:
	$(CC) $(CPU) -c -x assembler-with-cpp -o $@ $<
.s.o:
	$(CC) $(CPU) -c -x assembler-with-cpp -o $@ $<
.S.o:
	$(CC) $(CPU) -c -x assembler-with-cpp -o $@ $<

.c.o:
	$(CC)  $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu99   $(INCLUDE_PATHS) -o $@ $<

.cpp.o:
	$(CPP) $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu++98 -fno-rtti $(INCLUDE_PATHS) -o $@ $<



$(BUILDDIR)/$(PROJECT).elf: $(OBJECTS) $(SYS_OBJECTS)
	$(LD) $(LD_FLAGS) -T$(LINKER_SCRIPT) $(LIBRARY_PATHS) -o $@ $^ $(LIBRARIES) $(LD_SYS_LIBS) $(LIBRARIES) $(LD_SYS_LIBS)


$(BUILDDIR)/$(PROJECT).bin: $(BUILDDIR)/$(PROJECT).elf
	$(OBJCOPY) -O binary $< $@

$(BUILDDIR)/$(PROJECT).hex: $(BUILDDIR)/$(PROJECT).elf
	@$(OBJCOPY) -O ihex $< $@

$(BUILDDIR)/$(PROJECT).lst: $(BUILDDIR)/$(PROJECT).elf
	@$(OBJDUMP) -Sdh $< > $@

lst: $(BUILDDIR)/$(PROJECT).lst

size: $(BUILDDIR)/$(PROJECT).elf
	$(SIZE) $(BUILDDIR)/$(PROJECT).elf

DEPS = $(OBJECTS:.o=.d) $(SYS_OBJECTS:.o=.d)
-include $(DEPS)


