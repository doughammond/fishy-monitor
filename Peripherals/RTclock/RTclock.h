#pragma once

#include <time.h>

class RTclock
{
public:
    // Frequency values for the square wave output
    enum ESquareWaveRates
    {
        eSWR_1Hz = 0,
        eSWR_4kHz = 1,
        eSWR_8kHz = 2,
        eSWR_32Hz = 3
    };
    
    enum EClockType
    {
        eDS1311,
        eDS3231,
    };

public:
                            RTclock
                            (
                                I2C &               in_cI2C,
                                uint8_t             in_nAddress,
                                EClockType          in_eClockType
                            );
    virtual                 ~RTclock();

            bool            isTwelveHour();                 // true if set to a 12hr clock (adjust tm.tm_hour as appropraite)
    virtual bool            mapTime();                      // Maps RTC chip to C time.h time system
    virtual bool            getTime(tm & out_sTM);          // Get a TM structure directly
    virtual bool            setSquareWaveOutput
                            (
                                bool                in_bEnable,
                                ESquareWaveRates    in_nRateSelect
                            );
    virtual bool            setTime                         // Set time time using a TM structure (always starts)
                            (
                                const tm &          in_sTM,
                                bool                in_bTwelveHour
                            );

protected:
    static int              bcdToDecimal(int in_nBCD);
    static int              decimalToBcd(int in_nDecimal);
    virtual bool            read
                            (
                                uint8_t             in_nAddress,
                                char *              out_pBuffer,
                                int                 in_nLength
                            );
    virtual bool            setRunning(bool in_nEnable);
    virtual bool            write
                            (
                                uint8_t             in_nAddress,
                                const char *        in_pBuffer,
                                int                 in_nLength
                            );

private:
    static  const char *    m_aWeekDays[];              // Days of the week    
            bool            m_bTwelveHour;
            I2C &           m_cI2C;
            uint8_t         m_nAddress;
            EClockType      m_eClockType;
};
