#include "mbed.h"
#include "time_helper.h"

void GetTime(tm & out_sTM)
{
    time_t nTime = time(0);
    tm * pTM = ::localtime(&nTime);
    ::memcpy(&out_sTM,pTM,sizeof(out_sTM));
}

void SetTime(const tm & in_sTM)
{
    tm sTM = { 0 };
    memcpy(&sTM,&in_sTM,sizeof(sTM));
    
    time_t nTime = mktime(&sTM);
    set_time(nTime);    
}
