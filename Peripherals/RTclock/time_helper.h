#pragma once

#include <time.h>

void GetTime(tm & out_sTM);
void SetTime(const tm & in_sTM);
