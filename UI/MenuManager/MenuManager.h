#pragma once

#include "lcd.h"
#include "keys.h"

class MenuManager
{    
public:
    MenuManager
    (
        Module **               in_pModules,
        size_t                  in_nModules,
        LCD &                   in_cLCD,
        Keys &                  in_cKeys
    );    
    void loop();

protected:
    void changeModule(bool in_bUp);
    void createChars();
    void initialise();
    void processKeys(uint8_t in_nKeys);
    void setCursor
    (
        bool        in_bCursor,
        bool        in_bBlink
    );
    void setMode(Module::EModes in_eMode);    
    void showModules(bool in_bRefresh = false);
    void showTracking(bool in_bShow);
    void updateDisplay();

protected:
    Module **               m_pModules;
    size_t                  m_nModules;    
    LCD &                   m_cLCD;
    Keys &                  m_cKeys;
    Module::EModes          m_eMode;
    size_t                  m_nMenuPos;
    size_t                  m_nIndex;
    int                     m_nCursorX, m_nCursorY;
};
