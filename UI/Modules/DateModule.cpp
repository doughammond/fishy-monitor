#include "mbed.h"
#include "DateModule.h"
#include "time_helper.h"

const char * k_aWeekDays[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

DateModule::DateModule
(
    Serial &    in_cDisplay,
    RTclock &   in_cRTclock
)
    : Module(in_cDisplay)
    , m_cRTclock(in_cRTclock)
{
    ::memset(&m_sLastTM,0,sizeof(m_sLastTM));
}

DateModule::~DateModule()
{
}

void DateModule::change
(
    size_t      in_nIndex,
    bool        in_bUp
)
{
    tm sTM;
    
    // to get the current time information
    if (!m_cRTclock.getTime(sTM)) GetTime(sTM);
    bool bTwelveHour = m_cRTclock.isTwelveHour();

    enum ETime
    {
        eWeekDay = 0,
        eDayTen,
        eDaySingle,
        eMonthTen,
        eMonthSingle,
        eYearTen,
        eYearSingle,
    };    
    
    switch (in_nIndex)
    {
        case eWeekDay:      sTM.tm_wday += (in_bUp ? 1 : -1);  break;
        case eDayTen:       sTM.tm_mday += (in_bUp ? 1 : -1) * 10;  break;
        case eDaySingle:    sTM.tm_mday += (in_bUp ? 1 : -1);  break;
        case eMonthTen:     sTM.tm_mon += (in_bUp ? 1 : -1) * 10;  break;
        case eMonthSingle:  sTM.tm_mon += (in_bUp ? 1 : -1);  break;
        case eYearTen:      sTM.tm_year += (in_bUp ? 1 : -1) * 10;  break;
        case eYearSingle:   sTM.tm_year += (in_bUp ? 1 : -1);  break;
    }
    
    if (sTM.tm_wday < 0)    sTM.tm_wday = 0;
    if (sTM.tm_wday > 6)    sTM.tm_wday = 6;
    if (sTM.tm_mday < 1)    sTM.tm_mday = 1;
    if (sTM.tm_mday > 31)   sTM.tm_mday = 31;
    if (sTM.tm_mon < 0)     sTM.tm_mon = 0;
    if (sTM.tm_mon > 11)    sTM.tm_mon = 11;
    if (sTM.tm_year < 2000 - 1900) sTM.tm_year = 2000 - 1900;
    if (sTM.tm_year > 2099 - 1900) sTM.tm_year = 2099 - 1900;

    if (m_cRTclock.setTime(sTM,bTwelveHour))
    {
        m_cRTclock.mapTime();
    }
    else
    {
        SetTime(sTM);
    }
}
  
int DateModule::getCursorOffset(size_t & inout_nIndex)
{
    const int k_aCursor[] = { 2, 4, 5, 7, 8, 12, 13 };
    
    if ((int)inout_nIndex < 0) inout_nIndex = 0;
    if (inout_nIndex >= _countof(k_aCursor)) inout_nIndex = _countof(k_aCursor) - 1;
       
    return k_aCursor[inout_nIndex];
}

void DateModule::show(bool in_bRefresh)
{
    tm sTM;
    
    // to get the current time information
    if (!m_cRTclock.getTime(sTM)) GetTime(sTM);
    
    // if refreshing - only update if there's a change
    if (in_bRefresh)
    {
        // Check for change based on day (rest is irrelevant)
        if (sTM.tm_mday == m_sLastTM.tm_mday) return;
    }
    
    // Ensure internal struct has new TM data
    ::memcpy(&m_sLastTM,&sTM,sizeof(m_sLastTM));
    m_cDisplay.printf ("%s %02i/%02i/%04i    ", k_aWeekDays[sTM.tm_wday], sTM.tm_mday, sTM.tm_mon + 1, 1900 + sTM.tm_year);
}
