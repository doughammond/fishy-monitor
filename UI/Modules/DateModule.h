#pragma once

#include "module.h"
#include "RTclock.h"

class DateModule
    : public Module
{
public:
    DateModule
    (
        Serial &    in_cDisplay,
        RTclock &   in_cRTclock
    );
    virtual ~DateModule();
    
    virtual bool    canRefresh() { return true; }
    virtual void    change
                    (
                        size_t      in_nIndex,
                        bool        in_bUp
                    );
    virtual int     getCursorOffset(size_t & inout_nIndex);
    virtual void    show(bool in_bRefresh);
    
protected:
    RTclock &       m_cRTclock;
    tm              m_sLastTM;
};
