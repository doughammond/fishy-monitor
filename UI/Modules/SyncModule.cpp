#include "mbed.h"
#include "SyncModule.h"
#include "extra_chars.h"

SyncModule::SyncModule
(
    Serial &    in_cDisplay,
    RTclock &   in_cClock
)
    : Module(in_cDisplay)
    , m_cClock(in_cClock)
    , m_eMode(eModeLast)    
    , m_bSync(false)
{
}

SyncModule::~SyncModule()
{
}

void SyncModule::change
(
    size_t      /* in_nIndex */,
    bool        /* in_bUp */
)
{
    m_bSync = (m_bSync) ? false : true;
}

int SyncModule::getCursorOffset(size_t & inout_nIndex)
{
    return m_bSync ? 13 : 12;
}

void SyncModule::onModeChange(EModes in_eMode)
{
    EModes eOldMode = m_eMode;
    m_eMode = in_eMode;

    if (eModeChange != eOldMode) return;    
    if (eModeMenu != m_eMode) return;
    
    if (m_bSync)
    {
        // Sync the internal mbed clock (stdlib)
        m_cClock.mapTime();
        m_bSync = false;    
    }
}

void SyncModule::show(bool /*in_bRefresh*/)
{
    switch (m_eMode)
    {
        case eModeChange:
        case eModeSelect:
            m_cDisplay.printf("Sync Time? %s   ",m_bSync ? "yes" : "no");
            break;
            
        default:
            m_cDisplay.printf("Sync Time %c     ",eRight);
    }
}
