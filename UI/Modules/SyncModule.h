#pragma once

#include "module.h"
#include "RTclock.h"

class SyncModule
    : public Module
{
public:
    SyncModule
    (
        Serial &    in_cDisplay,
        RTclock &   in_cClock
    );
    virtual ~SyncModule();

    virtual void    change
                    (
                        size_t      in_nIndex,
                        bool        in_bUp
                    );
    virtual int     getCursorOffset(size_t & inout_nIndex);
    virtual void    onModeChange(EModes in_eMode);
    virtual void    show(bool in_bRefresh);

protected:
    RTclock &       m_cClock;
    EModes          m_eMode;
    bool            m_bSync;
};
