#include "mbed.h"
#include "TempModule.h"

#include "extra_chars.h"

#define MCP9808_I2CADDR_DEFAULT        (0x18 << 1)
#define MCP9808_REG_CONFIG             0x01

#define MCP9808_REG_CONFIG_SHUTDOWN    0x0100
#define MCP9808_REG_CONFIG_CRITLOCKED  0x0080
#define MCP9808_REG_CONFIG_WINLOCKED   0x0040
#define MCP9808_REG_CONFIG_INTCLR      0x0020
#define MCP9808_REG_CONFIG_ALERTSTAT   0x0010
#define MCP9808_REG_CONFIG_ALERTCTRL   0x0008
#define MCP9808_REG_CONFIG_ALERTSEL    0x0002
#define MCP9808_REG_CONFIG_ALERTPOL    0x0002
#define MCP9808_REG_CONFIG_ALERTMODE   0x0001

#define MCP9808_REG_UPPER_TEMP         0x02
#define MCP9808_REG_LOWER_TEMP         0x03
#define MCP9808_REG_CRIT_TEMP          0x04
#define MCP9808_REG_AMBIENT_TEMP       0x05
#define MCP9808_REG_MANUF_ID           0x06
#define MCP9808_REG_DEVICE_ID          0x07

TempModule::TempModule(Serial & in_cDisplay,I2C & in_nI2C,uint8_t in_nAddress)
    : Module(in_cDisplay)
    , m_cI2C(in_nI2C)
    , m_nAddress(in_nAddress)
    , m_bValid(false)
{
    uint16_t nManufacturer = read16(MCP9808_REG_MANUF_ID);// != 0x0054) return false;
    uint16_t nDeviceId = read16(MCP9808_REG_DEVICE_ID);//
    
    // Check to see if we need to autoscan for our device
    m_bValid = (0x0054 == nManufacturer || 0x0400 == nDeviceId);

//    bool bAutoScan = true;
//    m_nAddress = 0;
//    
//    while (bAutoScan && m_nAddress < 0x80)
//    {
//        nManufacturer = read16(MCP9808_REG_MANUF_ID);// != 0x0054) return false;
//        nDeviceId = read16(MCP9808_REG_DEVICE_ID);//
//        
//        bAutoScan = (0x0054 != nManufacturer || 0x0400 != nDeviceId);
//        m_nAddress++;
//    }
}

TempModule::~TempModule()
{
}

float TempModule::readTempC()
{
    uint16_t t = read16(MCP9808_REG_AMBIENT_TEMP);
    
    float temp = t & 0x0FFF;
    temp /=  16.0f;
    
    // check to see if signed bit set
    if (t & 0x1000) temp -= 0xff;
    
    return temp;
}

void TempModule::write16(uint8_t in_nRegister, uint16_t in_nValue)
{
    char aBuffer[3] = { in_nRegister, in_nValue >> 8, in_nValue & 0xFF };
    m_cI2C.write(MCP9808_I2CADDR_DEFAULT, aBuffer, sizeof(aBuffer));
}

uint16_t TempModule::read16(uint8_t in_nRegister)
{
    m_cI2C.write(m_nAddress, (const char *)&in_nRegister, sizeof(in_nRegister));
    
    char aReadBuffer[2];
    m_cI2C.read(m_nAddress, aReadBuffer, sizeof(aReadBuffer));  
    
    return (aReadBuffer[0] << 8) | aReadBuffer[1];
}

void TempModule::show(bool in_bRefresh)
{
    static time_t nTime = time(NULL) + 1;
    if (in_bRefresh || nTime < time(NULL))
    {
        m_cDisplay.printf("Room: %0.1f%cC     ",readTempC(),eDegree);
        
//        uint8_t nValue = read16(MCP9808_REG_MANUF_ID);        
//        m_cDisplay.printf("%x - %x - %x          ", m_nAddress, nValue,read16(MCP9808_REG_DEVICE_ID));
        
        nTime = time(NULL) + 1;
    }
}
