#pragma once

#include "mbed.h"
#include "module.h"

class TempModule
    : public Module
{
public:
    TempModule
    (
        Serial &    in_cDisplay,
        I2C &       in_cI2C,
        uint8_t     in_nAddress
    );
    virtual ~TempModule();
    
    virtual bool    canRefresh() { return true; }
    virtual bool    isValid() { return m_bValid; }
    virtual void    show(bool in_bRefresh);

protected:
    float       readTempC();
    uint16_t    read16(uint8_t in_nRegister);
    void        write16(uint8_t in_nRegister, uint16_t in_nValue);

    I2C &       m_cI2C;
    uint8_t     m_nAddress;
    bool        m_bValid;
};
