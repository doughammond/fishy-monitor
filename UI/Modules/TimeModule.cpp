#include "mbed.h"
#include "TimeModule.h"
#include "time_helper.h"

#if 0
    tm sTM;
    sTM.tm_sec = 0;
    sTM.tm_min = 0;
    sTM.tm_hour = 18;
    sTM.tm_mday = 9;
    sTM.tm_mon  = 8 - 1;
    sTM.tm_year = 2014 - 1900;
    sTM.tm_wday = 6;

    rtc.SetTime(sTM,true);
#endif


TimeModule::TimeModule
(
    Serial &    in_cDisplay,
    RTclock &   in_cRTclock
)
    : Module(in_cDisplay)
    , m_cRTclock(in_cRTclock)
{
    ::memset(&m_sLastTM,0,sizeof(m_sLastTM));
}

TimeModule::~TimeModule()
{
}

void TimeModule::change
(
    size_t      in_nIndex,
    bool        in_bUp
)
{
    tm sTM;
    
    // to get the current time information
    if (!m_cRTclock.getTime(sTM)) GetTime(sTM);
    bool bTwelveHour = m_cRTclock.isTwelveHour();

    enum ETime
    {
        eHourTen = 0,
        eHourSingle,
        eMinTen,
        eMinSingle,
        eSecondTen,
        eSecondSingle,
        eAmPm,
    };    
    
    switch (in_nIndex)
    {
        case eHourTen:      sTM.tm_hour += (in_bUp ? 1 : -1) * 10;  break;
        case eHourSingle:   sTM.tm_hour += (in_bUp ? 1 : -1); break;
        case eMinTen:       sTM.tm_min += (in_bUp ? 1 : -1) * 10;  break;
        case eMinSingle:    sTM.tm_min += (in_bUp ? 1 : -1); break;
        case eSecondTen:    sTM.tm_sec += (in_bUp ? 1 : -1) * 10;  break;
        case eSecondSingle: sTM.tm_sec += (in_bUp ? 1 : -1); break;
        case eAmPm:
            if (bTwelveHour)
            {
                if (in_bUp)
                {
                    if (sTM.tm_hour >= 12) bTwelveHour = !bTwelveHour; else sTM.tm_hour += 12;
                }
                else
                {
                    if (sTM.tm_hour >= 12) sTM.tm_hour -= 12; else bTwelveHour = !bTwelveHour;
                }
            }
            else
            {
                bTwelveHour = !bTwelveHour;
                if (in_bUp && sTM.tm_hour >= 12) sTM.tm_hour -= 12;
                if (!in_bUp && sTM.tm_hour < 12) sTM.tm_hour += 12;
            }                
            break;
    }
    
    if (sTM.tm_hour < 0)    sTM.tm_hour = 0;
    if (sTM.tm_hour > 23)   sTM.tm_hour = 23;
    if (sTM.tm_min < 0)     sTM.tm_min = 0;
    if (sTM.tm_min > 59)    sTM.tm_min = 59;
    if (sTM.tm_sec < 0)     sTM.tm_sec = 0;
    if (sTM.tm_sec > 59)    sTM.tm_sec = 59;

    if (m_cRTclock.setTime(sTM,bTwelveHour))
    {
        m_cRTclock.mapTime();
    }
    else
    {
        SetTime(sTM);
    }
}

int TimeModule::getCursorOffset(size_t & inout_nIndex)
{
    const int k_aCursor[] = { 0, 1, 3, 4, 6, 7, 10 };
    
    if ((int)inout_nIndex < 0) inout_nIndex = 0;
    if (inout_nIndex >= _countof(k_aCursor)) inout_nIndex = _countof(k_aCursor) - 1;
       
    return k_aCursor[inout_nIndex];
}

void TimeModule::show(bool in_bRefresh)
{
    tm sTM;
    const char * pUnits = "  ";
   
    // to get the current time information 
    if (m_cRTclock.getTime(sTM))
    {
        // Adjust for 12 hour clock
        if (m_cRTclock.isTwelveHour())
        {
            pUnits = (sTM.tm_hour < 12) ? "am":"pm";
            if (sTM.tm_hour > 12) sTM.tm_hour %= 12;
        }
    }
    else
    {
        // If failed get internal time (as at least that's something
        GetTime(sTM);
    }
    
    // if refreshing - only update if there's a change
    if (in_bRefresh)
    {        
        // Check for change based on second (rest is irrelevant)
        if (sTM.tm_sec == m_sLastTM.tm_sec) return;
    }
    
    // Ensure internal struct has new TM data
    ::memcpy(&m_sLastTM,&sTM,sizeof(m_sLastTM));

    m_cDisplay.printf ("%02i:%02i:%02i %s   ", sTM.tm_hour, sTM.tm_min, sTM.tm_sec, pUnits);    
}
