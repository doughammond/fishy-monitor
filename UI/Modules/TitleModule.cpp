#include "mbed.h"
#include "TitleModule.h"
#include "time_helper.h"

TitleModule::TitleModule
(
    Serial &    in_cDisplay,
    RTclock &   in_cRTclock
)
    : Module(in_cDisplay)
    , m_cRTclock(in_cRTclock)
{
    ::memset(&m_sLastTM,0,sizeof(m_sLastTM));
}

TitleModule::~TitleModule()
{
}

void TitleModule::show(bool in_bRefresh)
{
    tm sTM;
    
    // to get the current time information
    if (!m_cRTclock.getTime(sTM)) GetTime(sTM);
    
    // if refreshing - only update if there's a change
    if (in_bRefresh)
    {
        // Check for change based on hour (rest is irrelevant)
        if (sTM.tm_hour == m_sLastTM.tm_hour) return;
    }
    
    // Ensure internal struct has new TM data
    ::memcpy(&m_sLastTM,&sTM,sizeof(m_sLastTM));
    if (sTM.tm_hour < 6) m_cDisplay.printf("Night Time       ");
    else if (sTM.tm_hour < 12) m_cDisplay.printf("Morning Time      ");
    else if (sTM.tm_hour < 18) m_cDisplay.printf("Afternoon Time    ");
    else if (sTM.tm_hour < 21) m_cDisplay.printf("Evening Time      ");
    else m_cDisplay.printf("Bedtime           ");
}
