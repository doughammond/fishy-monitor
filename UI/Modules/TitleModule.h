#pragma once

#include "module.h"
#include "RTclock.h"

class TitleModule
    : public Module
{
public:
    TitleModule
    (
        Serial &    in_cDisplay,
        RTclock &   in_cRTclock
    );
    virtual ~TitleModule();
    
    virtual bool    canRefresh() { return true; }
    virtual void    show(bool in_bRefresh);
    
protected:
    RTclock &       m_cRTclock;
    tm              m_sLastTM;
};
