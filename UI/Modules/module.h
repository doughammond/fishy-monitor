#pragma once

#ifndef _countof
    #define _countof(a) (sizeof(a) / sizeof(a[0]))
#endif

class Module
{
public:
    enum EModes
    {
        eModeMenu = 0,
        eModeSelect,
        eModeChange,
        eModeLast
    };
    
public:
    Module(Serial & in_cDisplay);
    virtual ~Module();
    
    virtual bool    canRefresh() { return false; }
    virtual void    change
                    (
                        size_t      in_nIndex,
                        bool        in_bUp
                    )
                    { ; }
    virtual int     getCursorOffset(size_t & inout_nIndex)
                    { return -1; }
    virtual void    onModeChange(EModes in_eMode)
                    { ; }
    virtual void    show(bool in_bRefresh) = 0;
    
protected:
    Serial & m_cDisplay;
};
