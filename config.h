#pragma once


#define DEBUGTRACE_ENABLED OFF
#define DEBUGTRACE_BAUD 115200

#define I2C_BUS_SPEED 400000
#define I2C_SDA_PIN PTE0
#define I2C_SCL_PIN PTE1

#define MCP23017_ADDRESS 0x20

#define LCD_COLS 16
#define LCD_ROWS 2

#define ONEWIRE_PIN PTD5


// quick + small for debugging/testing
// #define TEMP_CAPTURE_INTERVAL 3
// #define TEMP_AVG_HISTORY_SIZE 20

// max history size determined by available memory
#define TEMP_CAPTURE_INTERVAL 45
#define TEMP_AVG_HISTORY_SIZE 3000
// 45 sec * 3000 samples = 135000sec = 37.5 hours history

// next two constants are in degrees C
#define TEMP_SAFE_THRESHOLD 1.0f
#define TEMP_WARN_THRESHOLD 2.5f
