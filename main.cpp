#include "mbed.h"

#include "config.h"

#include "Peripherals/AdaFruit_RGBLCDShield/AdaFruit_RGBLCDShield.h"
#include "Peripherals/DS18B20/DS18B20.h"

#include "LCD/charset.h"

#include "Util/DebugTrace.h"

// By declaring all vars at this scope, the compiler can actually
// tell us how much RAM is expected to be used:
//    text	   data	    bss	    dec	    hex	filename
//   50936	    244	  12776	  63956	   f9d4	_build//FishMonitor.elf
// Where:
// text - size of all the program code going into FLASH
// data - size of initialised variables, in FLASH then copied to RAM
// bss  - size of uninitialised data in RAM
namespace FishMonitor {

/**
 * Simple variables
 */

float temperature;
float tempHistory[TEMP_AVG_HISTORY_SIZE];

size_t i = 0, j = 0, k = 0;

// 16 chars LCD, + 3 chars fish, allows to scroll fish off the right edge of the screen
size_t stepChars = 20;

float logInterval = (float)TEMP_CAPTURE_INTERVAL;
float sleepTime = logInterval / (float)stepChars;
float tempSum = 0.f;


// LCD custom character bitmaps
enum Charset {
	CharDegree, CharTail, CharMiddle, CharHead
};


/**
 * Objects
 */

// Debugging interface
DebugTrace dt(DEBUGTRACE_ENABLED, TO_SERIAL);

// I2C bus interface
I2C cI2C(I2C_SDA_PIN, I2C_SCL_PIN);

// Port expander
MCP23017 cMCP(cI2C, MCP23017_ADDRESS << 1);

// LCD screen interface
Adafruit_RGBLCDShield cLCD(cMCP);

// 1-wire temperature sensor setup
DS18B20 tempSensor(ONEWIRE_PIN, false, false, false, NULL);

Timeout anim, temp, keys;

/**
 * Functions
 */

void initialise() {
	dt.baud(DEBUGTRACE_BAUD);
	dt.traceOut("Starting LCD...\r\n");

	cI2C.frequency(I2C_BUS_SPEED);
	cLCD.begin(LCD_COLS, LCD_ROWS);

	cLCD.createChar(CharDegree, Chars::symbols1[Chars::Degree]);
	cLCD.createChar(CharTail, Chars::fish[Chars::Tail]);
	cLCD.createChar(CharMiddle, Chars::fish[Chars::Middle]);
	cLCD.createChar(CharHead, Chars::fish[Chars::Head]);
	cLCD.setBacklight(LCD_BACKLIGHT_YELLOW);
	cLCD.setCursor(0, 0);
	cLCD.printf("Starting...");

	dt.traceOut("Starting Temp Sensor...\r\n");
	tempSensor.setResolution(twelveBit);
	tempSensor.initialize();

	dt.traceOut("Initialising mainloop...\r\n");
	temperature = tempSensor.readTemperature();
	for (k = 0; k < TEMP_AVG_HISTORY_SIZE; ++k) {
		if (k % 100 == 0) {
			dt.traceOut("initialising temp history; %d/%d\r\n", k, TEMP_AVG_HISTORY_SIZE);
		}
		// initialise the temperature history with the current reading
		tempHistory[k] = temperature;
		tempSum += temperature;
	}
}

void fish_animation() {
	// Print a fishy swimming across the top line
	dt.traceOut("Printing fishy...\r\n");
	cLCD.setCursor(0, 0);
	char str[] = "                    ";
	int8_t tailPos = i - 3;
	if (tailPos >= 0) {
		str[tailPos] = (char)CharTail;
	}
	int8_t midPos = i - 2;
	if (midPos >= 0) {
		str[midPos] = (char)CharMiddle;
	}
	int8_t headPos = i - 1;
	if (headPos >= 0) {
		str[headPos] = (char)CharHead;
	}
	cLCD.printf(str);

	i = (i + 1) % stepChars;

	// schedule the next call
	anim.attach(&fish_animation, sleepTime);
}

void temperature_logger() {
	dt.traceOut("Calculating average temp...\r\n");
	temperature = tempSensor.readTemperature();
	tempSum -= tempHistory[j];
	tempHistory[j] = temperature;
	tempSum += temperature;
	float avgTemp = tempSum / (float)TEMP_AVG_HISTORY_SIZE;

	dt.traceOut("Printing temp...\r\n");
	cLCD.setCursor(0, 1);
	// display the average temperature on the left side
	// display the current temperature on the right side
	cLCD.printf("%-0.1f%cC    %-0.1f%cC", avgTemp, CharDegree, temperature, CharDegree);

	dt.traceOut("Updating backlight colour...\r\n");
	float threshVal = fabs(avgTemp - temperature);
	if (threshVal < TEMP_SAFE_THRESHOLD) {
		// in safe zone
		cLCD.setBacklight(LCD_BACKLIGHT_GREEN);
	} else if (threshVal < TEMP_WARN_THRESHOLD) {
		// warning zone
		if (temperature < avgTemp) {
			// cool side
			cLCD.setBacklight(LCD_BACKLIGHT_CYAN);
		} else {
			// warm side
			cLCD.setBacklight(LCD_BACKLIGHT_MAGENTA);
		}
	} else if (temperature < avgTemp) {
		// too cold
		cLCD.setBacklight(LCD_BACKLIGHT_BLUE);
	} else {
		// too hot
		cLCD.setBacklight(LCD_BACKLIGHT_RED);
	}

	j = (j + 1) % TEMP_AVG_HISTORY_SIZE;

	// schedule the next call
	temp.attach(&temperature_logger, logInterval);
}

void button_handler() {
	Serial s(USBTX, USBRX);
	s.baud(DEBUGTRACE_BAUD);

	uint8_t state = cLCD.readButtons();
	if (state & BUTTON_SELECT) {
		s.printf("i step = %0.2f seconds\r\n", logInterval);
		s.printf("i\tTemperature\r\n");
		for (k = 0; k < TEMP_AVG_HISTORY_SIZE; ++k) {
			// print out the history from oldest to newest
			size_t h = (j + k) % TEMP_AVG_HISTORY_SIZE;
			s.printf("%d\t%0.2f\r\n", k, tempHistory[h]);
		}
	}

	// schedule the next call
	keys.attach(&button_handler, 0.2f);
}

} // namespace FishMonitor


int main() {
	using namespace FishMonitor;

	initialise();

	// calling each of these once is required to start the timers
	temperature_logger();
	button_handler();
	fish_animation();

	// main loop appears to do nothing since all functions are then timer invoked
	while (true) {
		wait(0.01);
	}
}
